//
//  ViewController.m
//  PhotoWebServer
//
//  Created by Relly on 2/10/15.
//  Copyright (c) 2015 BlueMate. All rights reserved.
//

#import "ViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HTTPServer.h"
#import <ExternalAccessory/ExternalAccessory.h>

@interface ViewController ()
{
    GCDWebServer* _webServer;
    HTTPServer *httpServer;
}

@end

@implementation ViewController
{
    UIImagePickerController *imagePicker;
    ALAssetsLibrary *libraryFolder;
    ALAssetsGroup *groupToAddTo;
    NSString *albumName;
    NSString *currentImageString;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initDeviceState];
    [self setAlbumName];
    [self initWebServer];
    [self performSelector:@selector(performImageCapture) withObject:nil afterDelay:1];
}

- (NSTimeInterval) timeStamp {
    return [[NSDate date] timeIntervalSince1970];
}

-(void)dealloc
{
    imagePicker = nil;
    libraryFolder = nil;
    groupToAddTo = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initWebServer
{
    
    // Create server
    _webServer = [[GCDWebServer alloc] init];
    
    // Add a handler to respond to GET requests on any URL
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *htmlPath = [path stringByAppendingPathComponent:@"index.html"];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:htmlPath isDirectory:NO];
    if (!fileExists) {
        NSString *htmlString = @"<html><body><h1>Most Recent Photo</h1><img src=\"mostrecentphoto.JPG\" height=\"400\" /></body></html>";
        [htmlString writeToFile:htmlPath atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
    }
    
    [_webServer addGETHandlerForBasePath:@"/" directoryPath:path indexFilename:@"index.html" cacheAge:3600 allowRangeRequests:YES];
    [_webServer startWithPort:8080 bonjourName:nil];
    self.lblUrl.text = [NSString stringWithFormat:@"%@", _webServer.serverURL];
    NSLog(@"Visit %@ in your web browser", _webServer.serverURL);
    
}

-(void)setAlbumName
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"ddMMMYYYY"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    albumName = [dateString stringByAppendingString:@"PhotoServer"];
}

-(void)performImageCapture
{
    [self setAlbumName];
    [self createDirectory];
    
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.showsCameraControls = NO;
        imagePicker.editing = YES;
        
        [self presentViewController:imagePicker animated:YES completion:^ {
            [self performSelector:@selector(imagePickertakePicture) withObject:nil afterDelay:1];
        }];
    }
    
}

-(void)imagePickertakePicture
{
    [imagePicker takePicture];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        
        UIImage *imageTmp = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [self fixOrientation:imageTmp];
        
        NSData *jpegData = UIImageJPEGRepresentation(image, 0.5);
        
        [jpegData writeToFile:[self getPathWithImageNameTimestamp] atomically:YES];
        [jpegData writeToFile:[self getPathWithImageNameMostRecent] atomically:YES];
        self.imgCurrent.image = [UIImage imageWithContentsOfFile:[self getPathWithImageNameMostRecent]];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Code here to support video if enabled
    }
    
    // Perform every 10 seconds
    [self performSelector:@selector(performImageCapture) withObject:nil afterDelay:10];
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}


-(void)createDirectory
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:albumName];
    
    // Create folder
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}


-(NSString*)getPathWithImageNameTimestamp
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *albumPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], albumName];
    NSString *imageName = [NSString stringWithFormat:@"%f.JPG", [self timeStamp]];
    
    return [albumPath stringByAppendingPathComponent:imageName];
}

-(NSString*)getPathWithImageNameMostRecent
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *mostrecentphotoPath = [paths objectAtIndex:0];
    NSString *imageName = [NSString stringWithFormat:@"%mostrecentphoto.JPG"];
    
    return [mostrecentphotoPath stringByAppendingPathComponent:imageName];
}

-(NSString*)getPathForWeb
{
    NSString* websitePath = [[NSBundle mainBundle] pathForResource:@"Web" ofType:@"html"];
    return websitePath;
}

-(void)deleteAllFilesFromDocuments
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *directory = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], @"13Feb2015PhotoServer"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    //NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:[paths objectAtIndex:0] error:nil];
    [fileMgr removeItemAtPath:[paths objectAtIndex:0] error:NULL];
    
    //for (NSString *filename in fileArray)  {
    //    [fileMgr removeItemAtPath:[directory stringByAppendingPathComponent:filename] error:NULL];
    //}
}

- (UIImage *)fixOrientation:(UIImage*)img {
    
    // No-op if the orientation is already correct
    if (img.imageOrientation == UIImageOrientationUp) return img;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (img.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, img.size.width, img.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, img.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, img.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (img.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, img.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, img.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, img.size.width, img.size.height,
                                             CGImageGetBitsPerComponent(img.CGImage), 0,
                                             CGImageGetColorSpace(img.CGImage),
                                             CGImageGetBitmapInfo(img.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (img.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,img.size.height,img.size.width), img.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,img.size.width,img.size.height), img.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *retImg = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return retImg;
}

-(void) initDeviceState
{
    UIDevice *device = [UIDevice currentDevice];
    device.batteryMonitoringEnabled = YES;

    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(deviceStateMessage:) name:UIDeviceBatteryStateDidChangeNotification object:device];
}

-(void)deviceStateMessage:(NSNotification*)notification
{
    UIDevice *device = [UIDevice currentDevice];
    UIDeviceBatteryState currentState = [device batteryState];
    NSString *stateString = nil;
    
    switch (currentState) {
        case UIDeviceBatteryStateCharging:
            stateString = @"Device plugged-in.\nCharging.";
            break;
        case UIDeviceBatteryStateFull:
            stateString = @"Device plugged-in.\nFull charged.";
            break;
        case UIDeviceBatteryStateUnplugged:
            stateString = @"Device unplugged.";
            break;
        default:
            stateString = @"Unknown";
            break;
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:stateString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}



@end
