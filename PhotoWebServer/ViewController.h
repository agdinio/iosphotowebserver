//
//  ViewController.h
//  PhotoWebServer
//
//  Created by Relly on 2/10/15.
//  Copyright (c) 2015 BlueMate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"


@interface ViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCurrent;
@property (weak, nonatomic) IBOutlet UILabel *lblUrl;

-(UIImage *)fixOrientation:(UIImage *)img;

@end
